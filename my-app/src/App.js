import React from 'react';
import axios from 'axios';
import validator from 'validator';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      firstName: { value: '', isValid: true, message: '' },
      lastName: { value: '', isValid: true, message: '' },
      email: { value: '', isValid: true, message: '' },
      cardNumber: '',
      cardExpiration: '',
      amount: 0,
      costs: 0,
      total: 0,
      coverCosts: false
    }
    this.checkboxCoverCosts = React.createRef();
  }

  handleSubmit(e) {
    e.preventDefault();
    if (this.formIsValid()) {
      axios({
        method: "POST",
        url: "http://localhost:3002/send",
        data: this.state
      }).then((response) => {
        console.log(JSON.stringify(response));
        if (response.data === 'success') {
          alert("Thank you for your donation.");
          this.resetForm()
        } else if (response.data.status === 'fail') {
          alert("Failed to submit. Contact support.")
        }
      });
    }
  }

  formIsValid = () => {
    const email = { ...this.state.email };
    let isGood = true;
    if (!validator.isEmail(email.value)) {
      email.isValid = false;
      email.message = 'Not a valid email address';
      isGood = false;
    }
    return isGood;
  }

  resetForm() {

    this.setState({
      firstName: { value: '', isValid: true, message: '' },
      lastName: { value: '', isValid: true, message: '' },
      email: { value: '', isValid: true, message: '' },
      cardNumber: '',
      cardExpiration: '',
      amount: 0,
      costs: 0,
      total: 0,
      coverCosts: false
    });
  }

  render() {
    return (
      <div className="App">
        <form id="contact-form" onSubmit={this.handleSubmit.bind(this)} method="POST">
          <div className="form-group">
            <label htmlFor="firstName">First Name</label>
            <input type="text" className="form-control" id="firstName" value={this.state.firstName.value} onChange={this.onFirstNameChange.bind(this)} autoFocus />
            <span className="help-block">{this.state.firstName.message}</span>
          </div>
          <div className="form-group">
            <label htmlFor="lastName">Last Name</label>
            <input type="text" className="form-control" id="lastName" value={this.state.lastName.value} onChange={this.onLastNameChange.bind(this)} />
            <span className="help-block">{this.state.lastName.message}</span>
          </div>
          <div className="form-group">
            <label htmlFor="email">Email Address</label>
            <input type="email" className="form-control" id="email" aria-describedby="emailHelp" value={this.state.email.value} onChange={this.onEmailChange.bind(this)} />
            <span className="help-block">{this.state.email.message}</span>
          </div>
          <div className="form-group">
            <label htmlFor="cardNumber">Credit Card Number</label>
            <input type="text" className="form-control" id="cardNumber" value={this.state.cardNumber.value} onChange={this.onCardNumberChange.bind(this)} />
            <span className="help-block">{this.state.email.message}</span>
          </div>
          <div className="form-group">
            <label htmlFor="cardExpiration">Credit Card Expiration Date (mm/yyyy)</label>
            <input type="text" className="form-control" id="cardExpiration" value={this.state.cardExpiration.value} onChange={this.onCardExpirationChange.bind(this)} />
            <span className="help-block">{this.state.email.message}</span>
          </div>
          <div className="form-group">
            <label htmlFor="amount">Donation Amount</label>
            <input type="text" className="form-control" id="amount" value={this.state.amount} onChange={this.onAmountChange.bind(this)} />
          </div>
          <div className="form-group">
            <label>
              <input ref={this.checkboxCoverCosts} type="checkbox" checked={this.state.coverCosts} onChange={this.onCoverCostsChange.bind(this)} />Cover Costs
            </label>
          </div>
          <div className="form-group">
            <label htmlFor="total">Total Amount</label>
            <input type="text" readOnly={true} className="form-control" id="total" value={this.state.total} />
          </div>
          <button type="submit" className="btn btn-primary">Donate</button>
        </form>
      </div>
    );
  }

  onFirstNameChange(event) {
    this.setState({ firstName: { value: event.target.value } });
  }

  onLastNameChange(event) {
    this.setState({ lastName: { value: event.target.value } });
  }

  onEmailChange(event) {
    this.setState({ email: { value: event.target.value } });
  }

  onCardNumberChange(event) {
    this.setState({ cardNumber: { value: event.target.value } });
  }

  onCardExpirationChange(event) {
    this.setState({ cardExpiration: { value: event.target.value } });
  }

  onAmountChange(event) {
    this.setState({ amount: event.target.value }, this.updateTotal);
  }

  onCoverCostsChange(event) {
    this.setState({ coverCosts: event.target.checked }, this.updateTotal);
    
  }

  updateTotal() {
    
    if (this.checkboxCoverCosts.current.checked === true) {
      console.log('checked')
      let amountFloat = parseFloat(this.state.amount);
      let costs = (amountFloat * .029) + .3;
      let newAmount = (amountFloat + costs).toFixed(2);
      this.setState({ total: newAmount });
      console.log(newAmount);
    }
    else {
      this.setState({ total: this.state.amount });
      console.log(this.state.amount);
    }
  }
}

export default App;
