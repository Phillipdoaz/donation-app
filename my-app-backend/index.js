var express = require('express');
var router = express.Router();
var cors = require('cors');
const sqlite3 = require('sqlite3').verbose();
let db = new sqlite3.Database(':memory:', (err) => {
    if (err) {
        return console.error(err.message);
    }
    console.log('Connected to the in-memory SQlite database.');
});

router.post('/send', (req, res, next) => {
    console.log('Data received: ' + JSON.stringify(req.body));
    try {
        db.run('CREATE TABLE IF NOT EXISTS donation(id INTEGER PRIMARY KEY, data TEXT)', function (err) {
            if (err) {
                throw err;
            }

            db.run('INSERT INTO donation(data) VALUES(?)', [JSON.stringify(req.body)], function (err) {
                if (err) {
                    throw err;
                }
                console.log('Data saved: ' + JSON.stringify(req.body));
                console.log('Fetching from database all records: ');
                db.all('SELECT * FROM donation', [], (err, rows) => {
                    if (err) {
                        throw err;
                    }
                    rows.forEach((row) => {
                    console.log(row.id + ", " + row.data);
                    });
                });
                res.status(200).send("success");
            });
        });
    }
    catch (err) {
        return console.log(err.message);
        res.status(400).send(err.message);
        return;
    }
})

const app = express()
app.use(cors())
app.use(express.json())
app.use('/', router)
console.log('Started server: http://localhost:3002/');
app.listen(3002)